//
// Created by mildo on 21-04-2022.
//

#ifndef C_SOCKET_PLAYGROUND_HEADER_H
#define C_SOCKET_PLAYGROUND_HEADER_H

#endif //C_SOCKET_PLAYGROUND_HEADER_H

#include <stdio.h>
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_PORT "80"
#define DEFAULT_ADDR "www.example.org"
#define DEFAULT_BUFLEN 512