//
// Created by mildo on 21-04-2022.
//

#include "../header/header.h"

/// \param address The address of the server we want to connect to.
/// \param port The port of the server we want to connect to, 80 is the default for websites.
/// \return A SOCKET that is used to send and receive data to and from the server.
SOCKET CreateSocket(char *address, char *port) {
    WSADATA wsaData;

    int iResult;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }

    struct addrinfo *result = NULL,
            *ptr = NULL,
            hints;

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC; /* Options: AF_UNSPEC to be unspecified, AF_INET for IPv4, AF_INET6 for IPv6 */
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    iResult = getaddrinfo(DEFAULT_ADDR, DEFAULT_PORT, &hints, &result);
    if (iResult != 0) {
        printf("getaddrinfo failed: %d\n", iResult);
        WSACleanup();
        return 1;
    }

    SOCKET ConnectSocket = INVALID_SOCKET;

    // Attempt to connect to the first address returned by
    // the call to getaddrinfo
    ptr = result;

    // Create a SOCKET for connecting to server
    ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
                           ptr->ai_protocol);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Error at socket(): %ld\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return 1;
    }

    // Connect to server.
    iResult = connect(ConnectSocket, ptr->ai_addr, (int) ptr->ai_addrlen);
    if (iResult == SOCKET_ERROR) {
        closesocket(ConnectSocket);
        ConnectSocket = INVALID_SOCKET;
    }

    // Should really try the next address returned by getaddrinfo
    // if the connect call failed
    // But for this simple example we just free the resources
    // returned by getaddrinfo and print an error message

    freeaddrinfo(result);

    if (ConnectSocket == INVALID_SOCKET) {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }

    return ConnectSocket;
}

/// \param connectSocket - The socket that has been created using the CreateSocket function.
/// \param sendBuffer - The message we want to send to the server, for example a GET request.
/// \param receiveFunction - The function that is called with the string and length of string received from the server.
/// \param receiveBufferSize -  The size of the buffer we use to receive data from the server.
/// \return An error code, 0 if successful.
int SendAndReceiveData(SOCKET connectSocket, const char *sendBuffer, void(*receiveFunction)(char *, int),
                       int receiveBufferSize) {
    int iResult;

    int recvbuflen;
    if (receiveBufferSize == -1) {
        recvbuflen = DEFAULT_BUFLEN;
    } else {
        recvbuflen = receiveBufferSize;
    }
    char* recvbuf = malloc(sizeof(char) * recvbuflen);

    // Send an initial buffer
    iResult = send(connectSocket, sendBuffer, (int) strlen(sendBuffer), 0);
    if (iResult == SOCKET_ERROR) {
        printf("send failed: %d\n", WSAGetLastError());
        closesocket(connectSocket);
        WSACleanup();
        return 1;
    }

    // shutdown the connection for sending since no more data will be sent
    // the client can still use the ConnectSocket for receiving data
    iResult = shutdown(connectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        printf("shutdown failed: %d\n", WSAGetLastError());
        closesocket(connectSocket);
        WSACleanup();
        return 1;
    }

    // Receive data until the server closes the connection
    do {
        iResult = recv(connectSocket, recvbuf, recvbuflen, 0);
        if (iResult > 0) {
            receiveFunction(recvbuf, iResult);
        } else if (iResult == 0) {
            printf("Connection closed\n");
        } else {
            printf("recv failed: %d\n", WSAGetLastError());
        }
    } while (iResult > 0);

    // cleanup
    closesocket(connectSocket);
    WSACleanup();

    free(recvbuf);

    return 0;
}