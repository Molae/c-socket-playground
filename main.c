#include "socket/socket.c"

void receiveData(char* data, int length) {
    for (int i = 0; i < length; i++) {
        printf("%c", data[i]);
    }
}

int main(void) {
    SOCKET socket = CreateSocket(DEFAULT_ADDR, DEFAULT_PORT);

    const char *sendbuf = "GET /index.html HTTP/1.1\r\n"
                          "Host: www.example.org\r\n\r\n";

    int err_code = SendAndReceiveData(socket, sendbuf, &receiveData, -1);

    return err_code;
}