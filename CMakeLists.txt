cmake_minimum_required(VERSION 3.22)
project(c_socket_playground C)

set(CMAKE_C_STANDARD 90)

add_executable(c_socket_playground main.c)
